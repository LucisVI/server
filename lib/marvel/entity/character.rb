module Marvel
  class Character

    attr_reader :id, :name, :description, :comics

    def initialize(args)
      @id = args[:id]
      @name = args[:name]
      @description = args[:description]
      @comics = args[:comics]
    end

  end
end