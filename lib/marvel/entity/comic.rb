module Marvel
  class Comic
    attr_reader :id, :title, :resource_uri, :series

    def initialize(args)
      @id = args[:id]
      @title = args[:title]
      @resource_uri = args[:resource_uri]
      @series = args[:series]
    end
  end
end