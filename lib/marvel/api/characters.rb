require 'pry'
require 'rest-client'
require 'json'
require 'marvel/conf/constants'
require 'digest'
require 'marvel/entity/character'

module Marvel
  class Characters

    CHARACTERS_URL = 'https://gateway.marvel.com/v1/public/characters'

    attr_reader :ts, :client, :characters, :character

    def initialize(**args)
      @client = args[:client] || define_client
      @ts = Time.now.strftime('%FT%T%:z')
    end

    def get_characters
      response = query_api("#{CHARACTERS_URL}")
      result = JSON.parse(response.body)['data']['results']
      result.map do |character|
        create_character character
      end
    end

    def get_character id
      response = query_api("#{CHARACTERS_URL}/#{id}")
      #result = JSON.parse(query_api("#{CHARACTERS_URL}/#{id}").body)['data']['results'].first
      character = JSON.parse(response.body)['data']['results'].first
      create_character character
    end

    def query_api url
      client.get(url, {params:
                          {apikey: Marvel::API_KEY,
                           ts: ts,
                           hash: hash
                          }
                     })
    end

    def create_character character
      Character.new(id: character['id'],
                    name: character['name'],
                    description: character['description'],
                    comics: character['comics']['items'])
    end

    private

    def hash
      @hash = Digest::MD5.hexdigest "#{ts}#{Marvel::PRIVATE_KEY}#{Marvel::API_KEY}"
    end

    def define_client
      @client = RestClient
    end

  end
end