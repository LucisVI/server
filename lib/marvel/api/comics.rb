require 'rest-client'
require 'pry'
require 'json'
require 'marvel/conf/constants'
require 'digest'
require 'marvel/entity/comic'

module Marvel
  class Comics

    COMICS_URL = 'https://gateway.marvel.com/v1/public/comics'

    attr_reader :ts, :client, :comics, :comic

    def initialize(**args)
      @client = args[:client] || define_client
      @ts = Time.now.strftime('%FT%T%:z')
      @comics = []
    end

    def get_comics
      res = client.get(COMICS_URL,
                       {params:
                            {apikey: Marvel::API_KEY,
                             ts: ts,
                             hash: hash}
                       })
      response = JSON.parse(res.body)['data']['results']
      response.each do |comic|
        comic = Comic.new(id: comic['id'],
                          title: comic['title'],
                          resource_uri: comic['resourceURI'],
                          series: comic['series'])
        comics << comic
      end
      comics
    end

    def get_comic id
      res = client.get("#{COMICS_URL}/#{id}",
                       {params:
                            {apikey: Marvel::API_KEY,
                             ts: ts,
                             hash: hash}
                       })
      response = JSON.parse(res.body)['data']['results'].first
      comic = Comic.new(id: response['id'],
                        title: response['title'],
                        resource_uri: response['resourceURI'],
                        series: response['series'])
    end

    private

    def hash
      @hash = Digest::MD5.hexdigest "#{ts}#{Marvel::PRIVATE_KEY}#{Marvel::API_KEY}"
    end

    def define_client
      @client = RestClient
    end

  end
end