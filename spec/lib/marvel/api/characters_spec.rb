require 'spec_helper'

describe Marvel::Characters do
  context "Gets marvel characters from the marvel api" do

    let(:characters) { subject.get_characters }
    let(:character) { subject.get_character 1011334 }

    it "gets a list of every character" do
      expect(characters).not_to be_empty
      characters.each do |c|
        expect(c).to respond_to :name
        expect(c).to respond_to :id
        expect(c).to respond_to :description
      end
    end

    it "gets an specific character" do
      expect(character.name).to eql('3-D Man')
      expect(character).to respond_to :name
      expect(character).to respond_to :id
      expect(character).to respond_to :description
    end

  end
end