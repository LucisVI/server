require 'spec_helper'

describe Marvel::Comics do
  context "Gets marvel comics from the marvel api" do
    before(:all) do
      @comics = Marvel::Comics.new
    end

    it "gets a list of every character" do
      expect(@comics.get_comics).not_to be_empty
      @comics.get_comics.each do |c|
        expect(c).to respond_to :id
        expect(c).to respond_to :title
        expect(c).to respond_to :resource_uri
        expect(c).to respond_to :series
      end
    end

    it "gets an specific character" do
      expect(@comics.get_comic(21464).title).to eql("Ultimate Spider-Man (Spanish Language Edition) (2000) #11")
      expect(@comics.get_comic(21464).resource_uri).to eql("http://gateway.marvel.com/v1/public/comics/21464")
      expect(@comics.get_comic(21464).series['name']).to eql("Ultimate Spider-Man (Spanish Language Edition) (2000 - 2001)")
    end

  end
end